class LeaderboardsController < ApplicationController
  def index
    @users = User.for_ranking
  end
end
