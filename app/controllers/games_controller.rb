class GamesController < ApplicationController
  def new
    @game = Game.new
  end

  def create
    @game = Game.new(game_params)

    if @game.save
      calculate_elo
      redirect_to games_path
    else
      render :new
    end
  end

  def index
    @games = Game.all
  end

  private

  def game_params
    params.require(:game).permit(
      :date_played,
      :opponent_id,
      :user_score,
      :opponent_score,
      :user_id
    )
  end

  def calculate_elo
    User.calculate_elo(@game)
  end
end
