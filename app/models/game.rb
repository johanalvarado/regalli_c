class Game < ActiveRecord::Base
  belongs_to :user
  belongs_to :opponent, class_name: 'User'

  validates_presence_of :date_played,
    :opponent_id,
    :user_score,
    :opponent_score

  validate :score_is_winnable?

  validate :valid_score?

  before_create :won_result

  def players
    if user_score > opponent_score
      { winner: user, loser: opponent }
    else
      { winner: opponent, loser: user }
    end
  end

  private

  def won_result
   self.won = true if user_score > opponent_score
  end

  def score_is_winnable?
    return true if user_score == 21 || opponent_score == 21
    errors.add(:score, "A score must to be 21")
  end

  def valid_score?
    return false if user_score.blank? || opponent_score.blank?
    return true if (user_score - opponent_score).abs >= 2
    errors.add(:score, "The difference of point must to be 2")
  end
end
