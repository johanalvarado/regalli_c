require 'active_support/concern'

module EloRanking
  extend ActiveSupport::Concern

  WIN_FEE = 0.3
  LOSER_FEE = 0.7
  FIRST_GAME_FEE = 10

  module ClassMethods
    def calculate_elo(game)
      @game = game
      @players = game.players
      @winner = @players[:winner]
      @loser = @players[:loser]
      set_winner_elo
      set_loser_elo
    end

    private

    def set_winner_elo
      @winner.update_column(:elo, (@winner.elo + winner_fee).round(1))
    end

    def set_loser_elo
      @loser.update_column(:elo, @loser.elo - loser_fee)
    end

    def winner_fee
      elo = @loser.elo
      winner_elo = elo * WIN_FEE
      return winner_elo if winner_elo > 0
      FIRST_GAME_FEE
    end

    def loser_fee
      elo = @loser.elo
      loser_elo = elo - (elo * LOSER_FEE)
      return loser_elo if loser_elo > 0
      0
    end
  end
end
