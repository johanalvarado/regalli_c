class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :trackable, :validatable
  include EloRanking

  has_many :games

  scope :opponents, -> (user) {
    User.where.not(id: user)
  }

  scope :for_ranking, ->{
    includes(:games).order(elo: :desc)
  }
end
