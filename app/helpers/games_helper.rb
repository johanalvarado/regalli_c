module GamesHelper
  def opponents
    User.opponents(current_user)
  end

  def score(game)
    "#{ game.user_score } - #{ game.opponent_score }"
  end

  def result(game)
    return "W" if game.won?
    "L"
  end
end
